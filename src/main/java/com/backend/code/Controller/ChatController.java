package com.backend.code.Controller;

import com.backend.code.Objects.ChatMessage;

import com.backend.code.Objects.ChatPage;
import com.backend.code.Objects.CountMessages;
import com.backend.code.Objects.chatUsers;
import com.backend.code.Objects.ChatMessage.MessageType;
import com.backend.code.Repoistry.ChatRepository;
import com.backend.code.Repoistry.PostRepository;

import com.backend.code.Objects.chatUsers;
import com.backend.code.Objects.ChatMessage.MessageType;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Arrays;
import java.util.Collections;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class ChatController {
	public static int loginUsers[] = new int[1000];
	@Autowired
	ChatRepository repo;
	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	@MessageMapping("/sendMessage")
	@SendTo("/topic/pubic")
	public ChatMessage sendMessage(@Payload final ChatMessage chatMessage) {
		return chatMessage;
	}

	@MessageMapping("/sendPrivateMessage")
	public void sendPrivateMessage(@Payload final ChatMessage chatMessage) throws SQLException {
			if(chatMessage.getType()==MessageType.CHAT)
			{
			List<CountMessages> count=repo.insertmessages(chatMessage);
			chatMessage.readcount=count.get(0).readcount;
			chatMessage.sendcount=count.get(0).sendcount;
			chatMessage.messageNumber=count.get(0).messagecount;
			String datetime=chatMessage.getMessageDate();
			chatMessage.setMessageTime(datetime.substring(11));
			chatMessage.setMessageDate("Today");
		    simpMessagingTemplate.convertAndSendToUser(
			String.valueOf(chatMessage.getReceiver()), "/queue", chatMessage);
		    simpMessagingTemplate.convertAndSendToUser(
			String.valueOf(chatMessage.getSender()), "/queue", chatMessage); 
			}
			else if(chatMessage.getType()==MessageType.READ)
			{
				 
			   List<CountMessages> count=repo.updateReadCount(chatMessage.getSender(),chatMessage.getReceiver());
			   chatMessage.readcount=count.get(0).readcount;
			   chatMessage.sendcount=count.get(0).sendcount;
			   chatMessage.messageNumber=count.get(0).messagecount;
				 simpMessagingTemplate.convertAndSendToUser(
			     String.valueOf(chatMessage.getReceiver()), "/queue", chatMessage);
		         simpMessagingTemplate.convertAndSendToUser(
			     String.valueOf(chatMessage.getSender()), "/queue", chatMessage);
			}
			else if(chatMessage.getType()==MessageType.TYPING)
			{
				simpMessagingTemplate.convertAndSendToUser(
			    String.valueOf(chatMessage.getReceiver()), "/queue", chatMessage);
			}

		}
	@MessageMapping("/addUser")
	@SendTo("/topic/pubic")
	public ChatMessage addUser(@Payload ChatMessage chatMessage,
			SimpMessageHeaderAccessor headerAccessor) {
		headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
		loginUsers[chatMessage.getSender()]=chatMessage.getSender();
		repo.updateSentMessages(chatMessage.getSender());
		return chatMessage;
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/getchatusers")
	public List<ChatPage> getmessages(@RequestAttribute("userid") int userId) {
		List<ChatPage> r = repo.getChatDetails(userId);
		Collections.reverse(r);
		return r;
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/getchatuser")
	public List<chatUsers> getmessagefromindivi(@RequestAttribute("userid") int userId,
			@RequestParam(value = "friendid") int friendId) {
			List<chatUsers>lis=	repo.getIndividualChat(userId, friendId);
			return lis;
	}

	@GetMapping("/test")
	public String testmessages() throws SQLException {
		ChatMessage chatusers = new ChatMessage();
		chatusers.setSender(1);
		chatusers.setReceiver(2);
		chatusers.setContent("dae");
		chatusers.setType(MessageType.CHAT);
		chatusers.setMessageDate("623546575765765767");
		chatusers.setMessageTime("987439988687687687");
		repo.insertmessages(chatusers);
		return "success";
	}

	
}
