package com.backend.code.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.code.Objects.IdName;
import com.backend.code.Objects.IdNameStatus;
import com.backend.code.Objects.IdNameStatus2;
import com.backend.code.Repoistry.FriendsRepository;
import com.backend.code.Repoistry.PostRepository;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FriendsController {

	@Autowired
	FriendsRepository repo;

	@CrossOrigin(origins = "*")
	@PostMapping("/showFriends")
	public List<IdNameStatus> showFriends(@RequestAttribute("userid") int userid) {
		return repo.showFriends(userid);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/showmembers")
	public List<IdNameStatus2> showMembers(@RequestAttribute("userid") int userid) {
		System.out.println(userid);
		return repo.showMembers(userid);
	}

	@PostMapping("/searchformembers/{pattern}")
	public List<IdNameStatus2> MemberSearch(@PathVariable("pattern") String patttern,
			@RequestAttribute("userid") int userid) {
		return repo.MemberSearch(patttern, userid);
	}

	@PostMapping("/searchforfriends/{pattern}")
	public List<IdName> FriendSearch(@PathVariable("pattern") String patttern, @RequestAttribute("userid") int userid) {
		return repo.FriendSearch(patttern, userid);
	}

	@CrossOrigin(origins = "*")
	@PostMapping("/addFriend")
	public String addFriend(@RequestBody com.backend.code.Objects.addFriend Af,
			@RequestAttribute("userid") int userId) {
		System.out.println(Af.getUser1());
		repo.addFriend(Af, userId);
		return "friend request sent";
	}

	@CrossOrigin(origins = "*")
	@PostMapping("/acceptFriend")
	public String acceptFriend(@RequestBody com.backend.code.Objects.addFriend Af,
			@RequestAttribute("userid") int userId) {
		repo.acceptFriend(Af, userId);
		return "friend Request accepted";
	}

	@CrossOrigin(origins = "*")
	@PostMapping("/removeFriend")
	public ResponseEntity<?> removeFriend(@RequestBody com.backend.code.Objects.addFriend Af,
			@RequestAttribute("userid") int userId) {
		try {
			repo.removeFriend(Af, userId);
		} catch (NullPointerException e) {
			return ResponseEntity.ok().body("no values are supplied");
		}
		return ResponseEntity.ok().body("friend Removed");
	}
	@CrossOrigin(origins = "*")
    @GetMapping("/showmembers/{gender}")
	public List<IdNameStatus2> showMembersByGender(@RequestAttribute("userid") int userid,@PathVariable("gender") String gender) {
		System.out.println(gender);
		return repo.showMembersByGender(userid,gender);
	}
}
