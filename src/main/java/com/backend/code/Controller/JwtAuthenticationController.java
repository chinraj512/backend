package com.backend.code.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.code.Objects.tokenResponse;
import com.backend.code.Objects.userpass;
import com.backend.code.Repoistry.UserProfileRepository;
import com.backend.code.config.JwtTokenUtil;
import com.backend.code.service.JwtUserDetailsService;


@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserProfileRepository repo;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody userpass authenticationRequest) throws Exception  {
		UserDetails userDetails = null;
		try {
			userDetails = userDetailsService.loadUserByUsername(authenticationRequest.email);
			if (userDetails.getUsername() == "InvalidUserName")
				return ResponseEntity.ok().body("given email is invalid");
			authenticate(authenticationRequest.email, authenticationRequest.password);
			String token = "";
			if (userDetails.getUsername() != " ")
				token = jwtTokenUtil.generateToken(userDetails);
			token=token+" "+Integer.toString(userDetailsService.getUserid());
			return ResponseEntity.ok().body(new tokenResponse(token));
		} catch (BadCredentialsException e) {
			return ResponseEntity.ok().body("incorrect password");
		}


	}

	private void authenticate(String username, String password) throws Exception {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	}
}

