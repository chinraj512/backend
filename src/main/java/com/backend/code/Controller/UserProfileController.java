package com.backend.code.Controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backend.code.Entity.UserDetails;
import com.backend.code.Entity.profile;
import com.backend.code.Exception.ResourceNotFoundException;
import com.backend.code.Objects.IdName;
import com.backend.code.Objects.IdNameStatus;
import com.backend.code.Objects.IdNameStatus2;
import com.backend.code.Objects.longProfile;
import com.backend.code.Objects.shortProfile;
import com.backend.code.Objects.userProfile;
import com.backend.code.Repoistry.PostRepository;
import com.backend.code.Repoistry.UserProfileRepository;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserProfileController {

	@Autowired
	UserProfileRepository repo;

	@GetMapping("/addprofile")
	public String profile(@RequestBody profile userprofile, @RequestAttribute("userid") int userid) {

		repo.profile(userprofile, userid);
		return "success";
	}

	@GetMapping("/user")
	public ResponseEntity<?> findById(@RequestParam(value = "userid") int userid) throws ResourceNotFoundException {
		List<userProfile> user = repo.findById(userid);
		if (user.size() == 0) {
			throw new ResourceNotFoundException("user not found");
		}
		return ResponseEntity.ok().body(user);
	}

	@PostMapping("/createUser")
	public ResponseEntity<?> insertUsersDetails(@Valid @RequestBody UserDetails user) throws NoSuchAlgorithmException {
		String result = "successfully accepted";
		System.out.println("controller");
		try {
			repo.insertUsersDetails(user);
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.ok().body("user is already registor");
		}
		return ResponseEntity.ok().body(result);
	}

	
	@GetMapping("/getbirthdaypeoples")
	public List<IdName> getBirthdayPeoples(@RequestAttribute(value = "userid") int userid) {
		return repo.getBirthdayPeoples(userid);
	}

	@GetMapping("/getshortprofile")
	public List<shortProfile> getShortProfile(@RequestAttribute(value = "userid") int userid) {
		System.out.println("sdfrf");
		return repo.getShortProfile(userid);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/getlongprofile/{profileid}")
	public longProfile getLongProfile(@RequestAttribute(value = "userid") int userid,@PathVariable ("profileid") int profileid) {
		System.out.println(profileid);
		return repo.getLongProfile(userid,profileid);
	}
	
	
	

}
