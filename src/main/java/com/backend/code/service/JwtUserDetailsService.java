package com.backend.code.service;

import java.util.ArrayList;
import java.util.List;

import com.backend.code.Objects.userpass;

import com.backend.code.Repoistry.PostRepository;
import com.backend.code.Repoistry.UserProfileRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired

	UserProfileRepository fr;

	public List<userpass> userDetails;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			userDetails = fr.findpassword(username);
			if (userDetails.size() == 0)
				return new User("InvalidUserName", " ", new ArrayList<>());
			return new User(userDetails.get(0).email, userDetails.get(0).password, new ArrayList<>());
		} catch (UsernameNotFoundException e) {
			return new User("InvalidUserName", " ", new ArrayList<>());
		}
	}

	public int getUserid() {
		return userDetails.get(0).userid;
	}
}

