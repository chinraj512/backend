package com.backend.code.Repoistry;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.aspectj.asm.internal.Relationship;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import com.backend.code.Entity.UserDetails;
import com.backend.code.Entity.profile;
import com.backend.code.Mapper.IdNameMapper;
import com.backend.code.Mapper.IdNameStatusMapper;
import com.backend.code.Mapper.RelationMapper;
import com.backend.code.Mapper.UserDetailsRowMapper;
import com.backend.code.Mapper.postMapper;
import com.backend.code.Mapper.shortMapper;
import com.backend.code.Mapper.shortProfileMapper;
import com.backend.code.Mapper.userpassMapper;
import com.backend.code.Objects.IdName;
import com.backend.code.Objects.IdNameStatus;
import com.backend.code.Objects.longProfile;
import com.backend.code.Objects.postResult;
import com.backend.code.Objects.shortProfile;
import com.backend.code.Objects.userProfile;
import com.backend.code.Objects.userpass;

@Repository
public class UserProfileRepository {

	NamedParameterJdbcTemplate template;

	public UserProfileRepository(NamedParameterJdbcTemplate template) {
		super();
		this.template = template;
	}

	public boolean findByEmailId(String email) {
		final String sql = "select userid,username from userdetails where email=:email";
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email);
		if (template.query(sql, param, new IdNameMapper()) != null)
			return true;
		else
			return false;
	}

	public int updatepassword(String password, String email) {
		final String sql = "update userdetails set password=:password where email=:email";
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("password", password);
		KeyHolder holder = new GeneratedKeyHolder();
		return (template.update(sql, param, holder));

	}

	public List<userProfile> findById(int id) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
		return template.query(
				"select * from userdetails left join profile on userdetails.userid=profile.userid left join ImageModel on profile.picId=ImageModel.picId where userdetails.userid=:id  ",
				param, new UserDetailsRowMapper());
	}

	public void insertUsersDetails(@RequestBody UserDetails user) throws NoSuchAlgorithmException {
		System.out.println("repo 1");
		final String sql = "INSERT INTO userdetails( username, password, email, phonenumber, dateofbirth, gender, age)VALUES (:username ,:password , :email, :phonenumber, :dateofbirth, :gender, :age);";
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashPassword = passwordEncoder.encode(user.getPassword());
		KeyHolder holder = new GeneratedKeyHolder();
		System.out.println("gugguguguguiguigugiugui");
		SqlParameterSource param = new MapSqlParameterSource().addValue("username", user.getUsername())
				.addValue("password", hashPassword).addValue("email", user.getEmail())
				.addValue("phonenumber", user.getPhonenumber()).addValue("dateofbirth", user.getDateofbirth())
				.addValue("gender", user.getGender()).addValue("age", user.getAge());
		template.update(sql, param, holder);

	}

	public void profile(profile userprofile, int userid) {
		final String sql = "INSERT INTO profile(user_id, school, college, degree,work, locality,picid)VALUES (:userid, :school ,:college , :degree, :work, :locality,:picid);";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid)
				.addValue("school", userprofile.getSchool()).addValue("college", userprofile.getCollege())
				.addValue("degree", userprofile.getDegree()).addValue("work", userprofile.getWork())
				.addValue("locality", userprofile.getLocality())
				.addValue("picid", userprofile.getImageModel().getPicid());
		template.update(sql, param, holder);
	}

	public List<userpass> findpassword(String username) {
		final String sql = "select userid,email,password from userdetails where email=:username";
		SqlParameterSource param = new MapSqlParameterSource().addValue("username", username);
		return template.query(sql, param, new userpassMapper());
	}

	@Scheduled(cron = "0 0 12 * * ?")
	public void updateAge() {
		final String sql = "update userdetails set age = age + 1 WHERE DATE_PART('day', dateofbirth) = date_part('day', CURRENT_DATE) AND DATE_PART('month', dateofbirth) = date_part('month', CURRENT_DATE) ";
		KeyHolder holder = new GeneratedKeyHolder();
		template.update(sql, null, holder);
	}

	public List<shortProfile> getShortProfile(int userid)
	{
		final String sql="select p.work,p.college,p.degree,p.school,p.locality,(select count(*) from friendsrelation where (user1=:userid or user2=:userid) and activity=1) as friends from profile p where userid=:userid;";
		SqlParameterSource param=new MapSqlParameterSource().addValue("userid", userid);
		return template.query(sql, param,new shortMapper());
	}

	public longProfile getLongProfile(int userid,int profileid) {
		final String sql1 = "select p.postid ,u.userid, p.status,p.location,p.commentcount,p.likecount,p.date,u.username,i.name,i.type,i.picbyte,(case when exists(select userid,postid from likec where userid=:userid and postid=p.postid) then true when not exists (select userid,postid from likec where userid=:userid and postid=p.postid) then false end) as liked from post p join userdetails u on p.userid=u.userid join image_model i on p.picid=i.picid where p.userid=:userid";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		List<postResult> posts = template.query(sql1, param, new postMapper());
		final String sql2 = "select ud.userid,ud.username,im.picbyte,im.picid,true as status from userdetails ud left join profile p on ud.userid=p.userid  left join image_model im on p.picid=im.picid  where (ud.userid in (select user1 as user from friendsrelation where user2=:userid union  select user2 as user from friendsrelation where user1=:userid)) ";
		SqlParameterSource param1 = new MapSqlParameterSource().addValue("userid", userid);
		List<IdNameStatus> friends = template.query(sql2, param1, new IdNameStatusMapper());
		final String sql3 = "select username,phonenumber,gender,email,dateofbirth,age,college,degree,locality,school,work,picbyte from userdetails u left join profile p on u.userid=p.userid left join image_model i on p.picid=i.picid where u.userid=:userid";
		List<shortProfile> res = template.query(sql3, param, new shortProfileMapper());
		final String sql4="select f,activity, (case " + 
				"when f.activity=0 and lastuser=:userid then 0" + 
				"when f.activity=0 and lastuser=:p then 1 " + 
				"when f.activity=1 then 2 " + 
				"when :userid=:p then 3 " + 
				"end  )as relationstatus " + 
				"from friendsrelation f " + 
				"where (user1=:userid and user2=:p) or (user1=:p  and user2=:userid)";
		SqlParameterSource param2=new MapSqlParameterSource().addValue("userid", userid).addValue("p", profileid);
		System.out.println(profileid+" "+userid+" "+param2.hasValue("p"));
		List<com.backend.code.Objects.Relationship> rel=template.query(sql4, param2, new RelationMapper());
		longProfile lp = new longProfile();
		lp.friends = friends;
		lp.posts = posts;
		lp.sp = res;
		lp.rel=rel;
		return lp;
	}

	public List<IdName> getBirthdayPeoples(int userid) {
		final String sql = "SELECT userid,username FROM userdetails WHERE DATE_PART('day', dateofbirth) = date_part('day', CURRENT_DATE) AND DATE_PART('month', dateofbirth) = date_part('month', CURRENT_DATE) and (userid!=:userid and userid in (select user1 as user from friendsrelation where user2=:userid union select user2 as user from friendsrelation where user1=:userid)) ";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		List<IdName> lis = template.query(sql, param, new IdNameMapper());
		return lis;
	}

}
