package com.backend.code.Repoistry;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.backend.code.Controller.ChatController;
import com.backend.code.Entity.UserDetails;
import com.backend.code.Mapper.IdNameMapper;
import com.backend.code.Mapper.IdNameStatusMapper;
import com.backend.code.Mapper.IdNameStatusMapper2;
import com.backend.code.Objects.IdName;
import com.backend.code.Objects.IdNameStatus;
import com.backend.code.Objects.IdNameStatus2;
import com.backend.code.Objects.addFriend;

@Repository
public class FriendsRepository {
	NamedParameterJdbcTemplate template;

	public FriendsRepository(NamedParameterJdbcTemplate template) {
		super();
		this.template = template;
	}

	public List<IdNameStatus2> getFriendRequest(int userid) {
		final String sql = "select ud.userid,ud.username,f.activity as status,im.picid,im.picbyte from friendsrelation f left join userdetails ud on ud.userid!=:userid and (ud.userid=f.user1 or ud.userid=f.user2) left join profile p on ud.userid=p.userid left join image_model im on p.picid=im.picid where (f.user1 = :userid or f.user2 = :userid) and (activity = 0) and lastuser != :userid;";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		return template.query(sql, param, new IdNameStatusMapper2());
	}

	public List<IdNameStatus2> getUserFriendRequest(int userid) {
		final String sql = "select ud.userid,ud.username,f.activity as status,im.picid,im.picbyte from friendsrelation f left join userdetails ud on ud.userid!=:userid and (ud.userid=f.user1 or ud.userid=f.user2) left join profile p on ud.userid=p.userid left join image_model im on p.picid=im.picid where (f.user1 = :userid or f.user2 = :userid) and (activity = 0) and lastuser  = :userid;";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		return template.query(sql, param, new IdNameStatusMapper2());
	}

	public void addFriend(addFriend af, int userId) {
		int user1;
		int user2;
		if (af.getUser1() > userId) {
			user1 = userId;
			user2 = af.getUser1();
		} else {
			user2 = userId;
			user1 = af.getUser1();
		}
		final String sql = "insert into friendsrelation(user1,user2,activity,lastuser) values (:user1,:user2,:relation,:lastAction)";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("user1", user1).addValue("user2", user2)
				.addValue("relation", af.getRelation()).addValue("lastAction", af.getLastAction());
		template.update(sql, param, holder);
	}

	public void removeFriend(com.backend.code.Objects.addFriend af, int userId) {
		final String sql = "delete from friendsrelation where user1=:userId and user2=:user1 or user1=:user1 and user2=:userId";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("user1", af.getUser1()).addValue("userId",
				userId);
		template.update(sql, param, holder);
	}

	public List<IdNameStatus> showFriends(int userid) {
		ChatController obj = new ChatController();
		obj.loginUsers[2] = 2;
		List<Integer> list = Arrays.stream(obj.loginUsers).boxed().collect(Collectors.toList());
		final String sql = "select ud.userid,username,im.picbyte,im.picid,(case \r\n"
				+ "						when exists(select userid from userdetails where ud.userid in(:users)) then true \r\n"
				+ "						when not exists (select userid from userdetails where ud.userid  in(:users)) then false end)\r\n"
				+ "						as status \r\n" + "from userdetails ud\r\n"
				+ "left join profile p on ud.userid=p.userid " + "left join image_model im on p.picid=im.picid "
				+ "where (ud.userid in (select user1 as user from friendsrelation where user2=:userId and activity=1 \r\n"
				+ "				  union\r\n"
				+ "				  select user2 as user from friendsrelation where user1=:userId and activity=1))";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userid).addValue("users", list);
		return template.query(sql, param, new IdNameStatusMapper());
	}

	public List<IdNameStatus2> showMembers(int userid) {
		final String sql = "select ud.userid,ud.username,im.picbyte,im.picid,(case when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=:userid ) then 1 when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=ud.userid) then 2  end) as status from userdetails ud left join profile p on  ud.userid = p.userid  left join image_model im on p.picid = im.picid  where ud.userid!=:userid and ud.userid not in (select u.user1 as user  from friendsrelation u where u.user2=:userid and u.activity=1 union select u.user2 as user  from friendsrelation u where u.user1=:userid and u.activity=1)";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		return template.query(sql, param, new IdNameStatusMapper2());
	}

	public List<IdNameStatus2> MemberSearch(String pattern, int userid) {
		final String sql = "select ud.userid,ud.username,im.picbyte,im.picid,(case when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=:userid ) then 1 when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=ud.userid) then 2  end) as status from userdetails ud left join profile p on  ud.userid = p.userid  left join image_model im on p.picid = im.picid where ((username like '%'||:pattern||'%') or (email like '%'||:pattern||'%') or (phonenumber like '%'||:pattern||'%')) and (ud.userid!=:userid and ud.userid not in (select user1 as user from friendsrelation where user2=:userid and (activity=0 or activity=2 or activity=3) union select user2 as user from friendsrelation where user1=:userid and(activity=0 or activity=2 or activity=3))) ";
		SqlParameterSource param = new MapSqlParameterSource().addValue("pattern", pattern).addValue("userid", userid);
		return template.query(sql, param, new IdNameStatusMapper2());
	}

	public List<IdName> FriendSearch(String pattern, int userid) {
		final String sql = "select userid,username  from userdetails where ((username like '%'||:pattern||'%') or (email like '%'||:pattern||'%') or (phonenumber like '%'||:pattern||'%')) and(not (userid!=:userid and userid not in (select user1 as user from friendsrelation where user2=:userid and activity=1 union select user2 as user from friendsrelation where user1=:userid and activity=1)) )";
		SqlParameterSource param = new MapSqlParameterSource().addValue("pattern", pattern).addValue("userid", userid);
		return template.query(sql, param, new IdNameMapper());
	}
	
	public void acceptFriend(addFriend af, int userId) {
        int user1;
        int user2;
        if (af.getUser1() > userId) {
            user1 = userId; 
            user2 = af.getUser1();
        } else {
            user2 = userId;
            user1 = af.getUser1();
        }
        final String sql = "update friendsrelation set lastuser=:lastAction,activity=:relation where user1=:user1 and user2=:user2";
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource param = new MapSqlParameterSource().addValue("user1", user1).addValue("user2", user2)
                .addValue("relation", af.getRelation()).addValue("lastAction", af.getLastAction());
        template.update(sql, param, holder);
    }
    
	public List < IdNameStatus2 > showMembersByGender(int userid,String gender) {
	    final String sql = "select ud.userid,ud.username,im.picbyte,im.picid,(case when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=:userid ) then 1 when exists(select * from friendsrelation where ((user1=ud.userid and user2=:userid) or (user1=:userid and user2=ud.userid)) and lastuser=ud.userid) then 2  end) as status from userdetails ud left join profile p on  ud.userid = p.userid  left join image_model im on p.picid = im.picid  where ud.userid!=:userid and ud.userid not in (select u.user1 as user  from friendsrelation u where u.user2=:userid and u.activity=1 union select u.user2 as user  from friendsrelation u where u.user1=:userid and u.activity=1) and ud.gender=':gender'";
	    SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid).addValue("gender", gender);
	    return template.query(sql, param, new IdNameStatusMapper2());
	   }

	public boolean findByEmailId(String email) {
		// TODO Auto-generated method stub
		return false;
	}

	public int updatepassword(String hashPassword, String email) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void insertUsersDetails(UserDetails realUser) {
		// TODO Auto-generated method stub
		
	}
}
