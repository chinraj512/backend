package com.backend.code.Repoistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.security.NoSuchAlgorithmException;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import com.backend.code.Objects.ChatMessage;
import com.backend.code.Objects.ChatPage;
import com.backend.code.Objects.CountMessages;
import com.backend.code.Objects.IdName;
import com.backend.code.Objects.IdNameStatus;
import com.backend.code.Objects.IdNameStatus2;
import com.backend.code.Objects.IdPattern;
import com.backend.code.Objects.MessageCount;
import com.backend.code.Objects.UserMessage;
import com.backend.code.Controller.ChatController;
import com.backend.code.Entity.ImageModel;
import com.backend.code.Entity.UserDetails;
import com.backend.code.Entity.profile;
import com.backend.code.Mapper.ChatPageMapper;
import com.backend.code.Mapper.CountMapper;
import com.backend.code.Mapper.IdNameMapper;
import com.backend.code.Mapper.IdNameStatusMapper;
import com.backend.code.Mapper.IdNameStatusMapper2;
import com.backend.code.Mapper.ImageMapper;
import com.backend.code.Mapper.MessageCountMapper;
import com.backend.code.Mapper.NameMapper;
import com.backend.code.Mapper.UserDetailsRowMapper;
import com.backend.code.Mapper.chatUsersMapper;
import com.backend.code.Mapper.commentMapper;
import com.backend.code.Mapper.messagemapper;
import com.backend.code.Mapper.postMapper;
import com.backend.code.Mapper.shortProfileMapper;
import com.backend.code.Mapper.userpassMapper;
import com.backend.code.Objects.addComment;
import com.backend.code.Objects.addFriend;
import com.backend.code.Objects.chatUsers;
import com.backend.code.Objects.longProfile;
import com.backend.code.Objects.messageobj;
import com.backend.code.Entity.post;
import com.backend.code.Objects.postResult;
import com.backend.code.Objects.shortProfile;
import com.backend.code.Objects.userProfile;
import com.backend.code.Objects.userpass;

@Repository
public class PostRepository {

	NamedParameterJdbcTemplate template;

	public PostRepository(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	public List<Integer> saveImage(ImageModel img) {
		KeyHolder holder = new GeneratedKeyHolder();
		final String sql = "insert into Image_model(name,type,picbyte) values(:name,:type,:picByte) returning picid";
		System.out.println(img.getPicByte());
		SqlParameterSource param = new MapSqlParameterSource().addValue("picId", img.getPicid())
				.addValue("name", img.getName()).addValue("type", img.getType()).addValue("picByte", img.getPicByte());
		return template.query(sql, param, new NameMapper());
	}

	public List<ImageModel> findImageByName(int imageId) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("picId", imageId);
		return template.query("select * from  image_model where picid=:picId", param, new ImageMapper());
	}

	public void addPost(post p, int userId) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm aa");
		Date present = new Date();
		String timestamp = df.format(present);
		final String sql = "insert into post(userid,status,location,likecount,commentcount,date,picid) values(:userId,:status,:location,:likeCount,:commentCount,:date,:picId)";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId)
				.addValue("picId", p.getPostId()).addValue("status", p.getStatus())
				.addValue("location", p.getLocation()).addValue("likeCount", p.getLikeCount())
				.addValue("commentCount", p.getCommentCount()).addValue("date", timestamp);
		template.update(sql, param, holder);
	}

	public void addLike(com.backend.code.Objects.addLike like, int userId) {
		final String sql1 = "insert into likec(postid,userid) values(:postId,:userId)";
		final String sql2 = "update post set likecount=likeCount+1 where postid=:postId";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", like.postId).addValue("userId",
				userId);
		template.update(sql1, param, holder);
		template.update(sql2, param, holder);
	}

	public void addComment(addComment comment, int userId) {
		final String sql1 = "insert into comment(postid,userid,comment) values(:postId,:userId,:comment)";
		final String sql2 = "update post set commentcount=commentcount+1 where postid=:postId";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", comment.postid)
				.addValue("userId", userId).addValue("comment", comment.comment);
		template.update(sql1, param, holder);
		template.update(sql2, param, holder);
	}

	public void removeLike(com.backend.code.Objects.addLike like, int userId) {
		final String sql = "delete from likec where postid=:postId and userid=:userId";
		final String sql2 = "update post set likecount=likecount-1 where postid=:postId";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", like.postId)
				.addValue("userId", userId).addValue("likeCount", like.likeCount);
		template.update(sql, param, holder);
		template.update(sql2, param, holder);
	}

	public void removeComment(com.backend.code.Objects.addComment comment, int userId) {
		final String sql = "delete from comment where postid=:postId and userid=:userId and commentid=:commentId";
		final String sql2 = "update post set commentcount=commentcount-1 where postid=:postId";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", comment.postid)
				.addValue("userId", userId).addValue("commentId", comment.commentid);
		template.update(sql, param, holder);
		template.update(sql2, param, holder);
	}

	public List<IdName> showLike(int userId) {
		final String sql = "select userid,username from userdetails where userid in (select userid from likec where postid=:postId)";
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", userId);
		return template.query(sql, param, new IdNameMapper());
	}

	public List<com.backend.code.Objects.displayComment> showComment(com.backend.code.Objects.addComment comment,
			int userId) {
		final String sql = "select c.commentid,c.userid,c.comment,username from comment as c join userdetails as u on c.userid=u.userid where postid=:postId";
		SqlParameterSource param = new MapSqlParameterSource().addValue("postId", comment.postid);
		return template.query(sql, param, new commentMapper());
	}

	public List<postResult> showPost(int userId) {
		System.out.println("from repo");
		final String sql = "select p.postid ,u.userid, p.status,p.location,p.commentcount,p.likecount,p.date,u.username,i.name,i.type,i.picbyte,(case when exists(select userid,postid from likec where userid=:userId and postid=p.postid) then true when not exists (select userid,postid from likec where userid=:userId and postid=p.postid) then false end) as liked from post p join userdetails u on p.userid=u.userid join image_model i on p.picid=i.picid where p.userid in (select user1 as user from friendsrelation where user2=:userId union select user2 as user from friendsrelation where user1=:userId) order by p.date desc";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId);
		return template.query(sql, param, new postMapper());
	}

	public void deletemessage() {
		String sql = "DROP TABLE IF EXISTS message,messagecount;";
		KeyHolder holder = new GeneratedKeyHolder();
		SqlParameterSource param = new MapSqlParameterSource();
		template.update(sql, param, holder);
	}
}
