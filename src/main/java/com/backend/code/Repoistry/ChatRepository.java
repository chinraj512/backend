package com.backend.code.Repoistry;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import com.backend.code.Controller.ChatController;
import com.backend.code.Mapper.ChatPageMapper;
import com.backend.code.Mapper.CountMapper;
import com.backend.code.Mapper.MessageCountMapper;
import com.backend.code.Mapper.chatUsersMapper;
import com.backend.code.Mapper.messagemapper;
import com.backend.code.Objects.ChatMessage;
import com.backend.code.Objects.ChatPage;
import com.backend.code.Objects.CountMessages;
import com.backend.code.Objects.MessageCount;
import com.backend.code.Objects.UserMessage;
import com.backend.code.Objects.chatUsers;
import com.backend.code.Objects.messageobj;
import com.backend.code.Objects.ChatMessage.MessageType;

@Repository
public class ChatRepository {

	NamedParameterJdbcTemplate template;

	public ChatRepository(NamedParameterJdbcTemplate template) {
		super();
		this.template = template;
	}

	public List<CountMessages> insertmessages(ChatMessage chatusers) throws SQLException {
		System.out.println("ggtegeegr");
		int user1;
		int user2;
		if (chatusers.getSender() > chatusers.getReceiver()) {
			user1 = chatusers.getReceiver();
			user2 = chatusers.getSender();
		} else {
			user1 = chatusers.getSender();
			user2 = chatusers.getReceiver();
		}
		ChatController users = new ChatController();
		CountMessages msg = new CountMessages();
		List<CountMessages> counts;
		int[] arr = users.loginUsers;
		SqlParameterSource param = new MapSqlParameterSource().addValue("user1", user1).addValue("user2", user2)
				.addValue("sender", chatusers.getSender());
		if (arr[chatusers.getReceiver()] == chatusers.getReceiver()) {
			final String sql1 = "update  messagecount set messagecount = messagecount+1 "
					+ "								, sendcount = sendcount + 1 "
					+ "								, sender=:sender "
					+ "where user1=:user1 and user2 =:user2 returning messagecount,sendcount,readcount\r\n";

			KeyHolder holder = new GeneratedKeyHolder();
			counts = template.query(sql1, param, new CountMapper());
			if (counts.size() == 0) {
				final String sql2 = "INSERT INTO public.messagecount(\r\n"
						+ "user1, user2, messagecount,sendcount,readcount,sender)\r\n"
						+ "select :user1 ,:user2 , 1 , 1 ,0 ,:sender\r\n"
						+ "where not exists(select 1 from messagecount where user1=:user1 and user2=:user2) returning messagecount,sendcount,readcount;\r\n";
				counts = template.query(sql2, param, new CountMapper());
			}
		} else {
			final String sql1 = "update  messagecount set messagecount = messagecount+1 "
					+ "								, sender=:sender "
					+ "where user1=:user1 and user2 =:user2 returning messagecount,sendcount,readcount\r\n";
			KeyHolder holder = new GeneratedKeyHolder();
			counts = template.query(sql1, param, new CountMapper());
			if (counts.size() == 0) {
				final String sql2 = "INSERT INTO public.messagecount(\r\n"
						+ "user1, user2, messagecount,sendcount,readcount,sender)\r\n"
						+ "select :user1 ,:user2 , 1 , 0 ,0 ,:sender\r\n"
						+ "where not exists(select 1 from messagecount where user1=:user1 and user2=:user2) returning messagecount,sendcount,readcount;\r\n";
				counts = template.query(sql2, param, new CountMapper());
			}
		}
		List<Integer> messagenum = template.query(
				"select messagecount from messagecount where user1=:user1 and user2=:user2", param,
				new messagemapper());
		String sql3 = "Insert into message (user1,user2,messagenum,message,createdtime,sender) values(:user1,:user2,:messagenum,:message,:createdtime,:senderid);";
		SqlParameterSource param1 = new MapSqlParameterSource().addValue("user1", user1).addValue("user2", user2)
				.addValue("message", chatusers.getContent()).addValue("messagenum", messagenum.get(0))
				.addValue("senderid", chatusers.getSender()).addValue("createdtime", chatusers.getMessageDate());
		KeyHolder holder = new GeneratedKeyHolder();
		template.update(sql3, param1, holder);
		return counts;
	}

	public List<messageobj> getmessages(int user1, int user2) {
		int temp;
		if (user1 > user2) {
			temp = user1;
			user1 = user2;
			user2 = temp;
		}
		final String sql = "select * from messages where user1=:user1 and user2=:user2";
		SqlParameterSource param = new MapSqlParameterSource().addValue("user1", user1).addValue("user2", user2);
		return template.query(sql, param, new UserMessage());
	}

	public List<ChatPage> getChatDetails(int realuser) {
		final String sql = "select u.username,u.userid,mc.messagecount,m.message,m.messagenum,m.sender,m.createdtime,mc.sendcount,mc.readcount,p.picid,ig.name,ig.picbyte,ig.type from messagecount mc inner join message m on (mc.user1=m.user1 and mc.user2=m.user2) inner join userdetails u on (mc.user1 !=1 and mc.user1=u.userid or mc.user2=u.userid and mc.user2 !=1) left join profile p on u.userid=p.userid left join image_model ig on p.picid=ig.picid  where (mc.user1=1 or mc.user2=1) and mc.messagecount=m.messagenum";
		SqlParameterSource param = new MapSqlParameterSource().addValue("realuser", realuser);
		List<ChatPage> lis=template.query(sql, param, new ChatPageMapper());
		return lis;
	}

	public void updateSentMessages(int userid) {
		final String sql = "update  messagecount set sendcount = messagecount\r\n"
				+ "	where  sendcount != messagecount and (user1=:userid or user2=:userid) and sender!=:userid;\r\n";
		SqlParameterSource param = new MapSqlParameterSource().addValue("userid", userid);
		KeyHolder holder = new GeneratedKeyHolder();
		template.update(sql, param, holder);
	}

	public List<CountMessages> updateReadCount(int senderid, int receiverid) {
		String sql = "update  messagecount set readcount = messagecount\r\n"
				+ "	where (user1=:sender and user2=:receiver) or (user1=:receiver and user2=:sender) returning messagecount,sendcount,readcount";
		SqlParameterSource param = new MapSqlParameterSource().addValue("sender", senderid).addValue("receiver",
				receiverid);
		KeyHolder holder = new GeneratedKeyHolder();
		List<CountMessages> counts = template.query(sql, param, new CountMapper());
		return counts;
	}

	public List<chatUsers> getIndividualChat(int userId, int friendId) {
		final String sql = "select *,:readcount as readcount, :sendcount as sendcount  from message where ((user1=:friendId and user2=:userId) or (user1=:userId and user2=:friendId))";
		List<chatUsers> chatuser;
		SqlParameterSource param = new MapSqlParameterSource().addValue("userId", userId).addValue("friendId",
				friendId);
		final String sql2 = "select readcount, sendcount from messagecount where (user1=:friendId and user2=:userId) or (user1=:userId and user2=:friendId)";
		List<MessageCount> ob;
		ob = template.query(sql2, param, new MessageCountMapper());
		if (ob.size() != 0) {
			SqlParameterSource param1 = new MapSqlParameterSource().addValue("userId", userId)
					.addValue("friendId", friendId).addValue("readcount", ob.get(0).readcount)
					.addValue("sendcount", ob.get(0).sendcount);

			chatuser = template.query(sql, param1, new chatUsersMapper());
			return chatuser;
		}
		chatuser = new LinkedList<>();
		return chatuser;
	}

}
