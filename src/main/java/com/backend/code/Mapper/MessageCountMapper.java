package com.backend.code.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.backend.code.Objects.MessageCount;

public class MessageCountMapper implements RowMapper<MessageCount>{
	MessageCount obj=new MessageCount();
	@Override
	public MessageCount mapRow(ResultSet rs, int rowNum) throws SQLException {
		obj.readcount=rs.getInt("readcount");
		obj.sendcount=rs.getInt("sendcount");
		return obj;
	}

}
