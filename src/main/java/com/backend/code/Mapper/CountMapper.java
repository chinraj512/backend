package com.backend.code.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.backend.code.Entity.ImageModel;
import com.backend.code.Objects.CountMessages;

public class CountMapper  implements RowMapper<CountMessages>{

	@Override
	public CountMessages mapRow(ResultSet rs, int rowNum) throws SQLException {
		CountMessages obj=new CountMessages();
		obj.messagecount=rs.getInt("messagecount");
		obj.sendcount=rs.getInt("sendcount");
		obj.readcount=rs.getInt("readcount");
		return obj;
	}

	

}